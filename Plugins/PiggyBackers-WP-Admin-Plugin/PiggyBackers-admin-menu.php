<?php  # -*- coding: utf-8 -*-
/**
 * Plugin Name: PiggyBacker Admin Menu
 * Description: Admin Menus for Viewing and Editing PiggyBacker Data
 * Version:     2014.09.15
 * Author:      Jon Allegre | Slalom Consulting
 * Author URI:  http://www.slalom.com
 */

add_action( 'admin_menu', array ( 'PiggyBacker_Admin_Page', 'admin_menu' ) );


class PiggyBacker_Admin_Page
{
	/**
	 * Register the pages and the style and script loader callbacks.
	 */
	public static function admin_menu()
	{
		
		global $wpdb;

	    $table_name = $wpdb->prefix . 'pikeup_piggybackers';
	
	    $sql = 'CREATE TABLE ' . $table_name . '(PKID INT NOT NULL AUTO_INCREMENT
	    ,TRANSACTION_ID VARCHAR(255) NOT NULL
	    ,DONOR_PUBLICATION_NAME VARCHAR(256)
	    ,TOTAL_HOOFS smallint(1) NOT NULL
	    ,TOTAL_CHARMS smallint(1) NOT NULL
	    ,DONATION_AMOUNT DECIMAL(7,2) NOT NULL
	    ,CREATED_DATE datetime NOT NULL
	    ,LAST_MODIFIED_DATE datetime
	    ,CREATED_BY VARCHAR(100) NOT NULL
	    ,PRIMARY KEY(PKID));';
	
	    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	    dbDelta( $sql );

	    $select_count_query = 'select count(*) from wp_pikeup_piggybackers;';
	    $count_of_rows_in_piggybacker_table = $wpdb->get_var($select_count_query, 0, 0);
	    if($count_of_rows_in_piggybacker_table == 0)
	    {
	    	self::insertInitialDataIntoPiggyBackers($table_name);
	    }
		
			//add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );		
/*			WP Menu Positions
			 2 Dashboard
			 4 Separator
			 5 Posts
			 10 Media
			 15 Links
			 20 Pages
			 25 Comments
			 59 Separator
			 60 Appearance
			 65 Plugins
			 70 Users
			 75 Tools
			 80 Settings
			 99 Separator
*/
		$main = add_menu_page(
			'Piggy Backers',                         // page title
			'Piggy Backers',                         // menu title
			// Change the capability to make the pages visible for other users.
			// See http://codex.wordpress.org/Roles_and_Capabilities
			'manage_options',                  // capability
			'PiggyBackersMain',                         // menu slug
			array ( __CLASS__, 'renderPiggyBackersMainPage'),  // callback function
			plugin_dir_url( __FILE__ ) . 'PiggyBackers-Icon.png',  //Menu Icon
			6 //Menu Postion
		);
		
		$sub = add_submenu_page(
			'PiggyBackersMain',                         // parent slug
			'Edit Piggy Backers',                     // page title
			'Edit Piggy Backers',                     // menu title
			'manage_options',                  // capability
			'PiggyBackersEdit',                     // menu slug
			array ( __CLASS__, 'renderPiggyBackersEditPage' ) // callback function, same as above
		);

		/* See http://wordpress.stackexchange.com/a/49994/73 for the difference
		 * to "'admin_enqueue_scripts', $hook_suffix"
		 */
		foreach ( array ( $main, $sub ) as $slug )
		{
			// make sure the style callback is used on our page only
			add_action(
				"admin_print_styles-$slug",
				array ( __CLASS__, 'enqueue_style' )
			);
			// make sure the script callback is used on our page only
			add_action(
				"admin_print_scripts-$slug",
				array ( __CLASS__, 'enqueue_script' )
			);
		}
	}

	/**
	 * Main Page Rendering
	 * @return  void
	 */
	public static function renderPiggyBackersMainPage()
	{
		global $title;
		print '<div class="wrap">';
		print '<p class="piggyBacker-pageTitle">';
		print '<img style="border-width:0px; vertical-align:middle;" src="' . plugin_dir_url( __FILE__ ) . 'PiggyBackers-Icon-Large.png' . '" width="30" height="30" alt="Piggy Backers" title="Piggy Backers" />';
		print '&nbsp;&nbsp;' . $title;
		print '</p>';			
		print '<div class="piggyBacker-panel">';
		print "<h2>PikeUp! Campaign Overview</h2>";
		global $wpdb;

		$tableName = $wpdb->prefix."pikeup_piggybackers";
		
		$totalPiggyBackers = $wpdb->get_var("select count(transaction_id) from $tableName", 0, 0);
		$totalAnonymous = $wpdb->get_var("select count(transaction_id) from $tableName where LOWER(DONOR_PUBLICATION_NAME) = 'anonymous'", 0, 0);		
		$totalNoGift = $wpdb->get_var("select count(transaction_id) from $tableName where total_hoofs = 0 and total_charms = 0", 0, 0);		
		$totalHoofsSold = $wpdb->get_var("select sum(total_hoofs) from $tableName", 0, 0);
		$totalCharmsSold = $wpdb->get_var("select sum(total_charms) from $tableName", 0, 0);
		$totalDonations = $wpdb->get_var("select sum(DONATION_AMOUNT) from $tableName", 0, 0);

		print "<p class='piggyBacker-totals'>Total Piggy Backers: &nbsp;&nbsp;<strong>$totalPiggyBackers </strong></p>";
		print "<p class='piggyBacker-totals'>Total Hoofs: &nbsp;&nbsp;<strong>$totalHoofsSold </strong></p>";
		print "<p class='piggyBacker-totals'>Total Charms: &nbsp;&nbsp;<strong>$totalCharmsSold </strong></p>";
		print "<p class='piggyBacker-totals'>Total No Gift (Donation Only): &nbsp;&nbsp;<strong>$totalNoGift </strong></p>";
		print "<p class='piggyBacker-totals'>Anonymous Donations: &nbsp;&nbsp;<strong>$totalAnonymous </strong></p>";
		print "<p class='piggyBacker-totals'>Total Donations: &nbsp;&nbsp;<strong>"; 
		setlocale(LC_MONETARY, "en_US");	
		print money_format("%10.2n", $totalDonations); 
		print "</strong></p>";		
		

		
		print '</div></div>';
	}

	public static function renderPiggyBackersEditPage()
	{
		global $title;

		print '<div class="wrap">';
		print '<p class="piggyBacker-pageTitle">';
		print '<img style="border-width:0px; vertical-align:middle;" src="' . plugin_dir_url( __FILE__ ) . 'PiggyBackers-Icon-Large.png' . '" width="30" height="30" alt="Piggy Backers" title="Piggy Backers" />';
		print '&nbsp;&nbsp;' . $title;
		print '</p>';			
		print '<div class="piggyBacker-panel">';
	
		require_once("mte/mte.php");
		$tabledit = new MySQLtabledit();
		
		# database settings:
		$tabledit->database = 'pike_up_db';
		$tabledit->host = 'localhost';
		$tabledit->user = 'pikeuser';
		$tabledit->pass = '0J5A#;+~y15T';
		
		# table of the database
		$tabledit->table = 'wp_pikeup_piggybackers';
		
		# the primary key of the table (must be AUTO_INCREMENT)
		$tabledit->primary_key = 'PKID';
		
		# the fields you want to see in "list view"
		$tabledit->fields_in_list_view = array('PKID', 'TRANSACTION_ID','DONOR_PUBLICATION_NAME','TOTAL_HOOFS','TOTAL_CHARMS','DONATION_AMOUNT','CREATED_DATE','CREATED_BY');
		
		
		
				#####################
				# optional settings #
				#####################
		
		
		# language (en of nl)
		$tabledit->language = 'en';
		
		# numbers of rows/records in "list view"
		$tabledit->num_rows_list_view = 25;
		
		# required fields in edit or add record
		$tabledit->fields_required = array('TRANSACTION_ID','DONOR_PUBLICATION_NAME','TOTAL_HOOFS','TOTAL_CHARMS','DONATION_AMOUNT','CREATED_DATE','LAST_MODIFIED_DATE','CREATED_BY');
		
		# help text 
		$tabledit->help_text = array(
			'TRANSACTION_ID' => "",
			'DONOR_PUBLICATION_NAME' => '',
			'TOTAL_HOOFS' => '',
			'TOTAL_CHARMS' => '',
			'DONATION_AMOUNT' => '',
			'CREATED_DATE' => '',
			'CREATED_BY' => ''
		);
		
		# visible name of the fields
		$tabledit->show_text = array(
			'PKID' => 'ID',
			'TRANSACTION_ID' => 'Transaction',
			'DONOR_PUBLICATION_NAME' => 'Publication name',
			'TOTAL_HOOFS' => 'Hoofs',
			'TOTAL_CHARMS' => 'Charms',
			'DONATION_AMOUNT' => 'Total Amount',
			'CREATED_DATE' => 'Created',
			'LAST_MODIFIED_DATE' => 'Modified',			
			'CREATED_BY' => 'Created By'
		);
		
		$tabledit->width_editor = '100%';
		$tabledit->width_input_fields = '500px';
		$tabledit->width_text_fields = '498px';
		$tabledit->height_text_fields = '200px';
		
		# warning no .htacces ('on' or 'off')
		$tabledit->no_htaccess_warning = 'off';
		
		
		####################################
		# connect, show editor, disconnect #
		####################################		
		
		$tabledit->database_connect();
		
		$tabledit->do_it();
		
		$tabledit->database_disconnect();
	
			
		print '</div></div>';	
	}

	/**
	 * Load stylesheet on our admin page only.
	 *
	 * @return void
	 */
	public static function enqueue_style()
	{
		wp_register_style(
			'PiggyBackers_css',
			plugins_url( 'PiggyBackers.css', __FILE__ )
		);
		wp_enqueue_style( 'PiggyBackers_css' );
		
/*
		wp_register_style(
			'mte_css',
			plugins_url( 'mte.css', __FILE__ )
		);
		wp_enqueue_style( 'mte_css' );
*/
	}

	/**
	 * Load JavaScript on our admin page only.
	 *
	 * @return void
	 */
	public static function enqueue_script()
	{
		wp_register_script(
			'PiggyBackers_js',
			plugins_url( 'PiggyBackers.js', __FILE__ ),
			array(),
			FALSE,
			TRUE
		);
		wp_enqueue_script( 'PiggyBackers_js' );
		
/*
		wp_register_script(
			'dgscripts_js',
			plugins_url( 'dgscripts.js', __FILE__ ),
			array(),
			FALSE,
			TRUE
		);
		wp_enqueue_script( 'dgscripts_js' );
*/
	}
	
	public static function renderGrid()
	{
//		include (plugin_dir_url( __FILE__ ) . "phpmydatagrid.class.php");
		
	}

	public static function insertInitialDataIntoPiggyBackers($table_name)
	{
        global $wpdb;

				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-1', 
				'DONOR_PUBLICATION_NAME' => 'Jody Foster and John Ryan', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-2', 
				'DONOR_PUBLICATION_NAME' => 'The Joshua Green Foundation', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-3', 
				'DONOR_PUBLICATION_NAME' => 'Dorothy Bullitt', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-4', 
				'DONOR_PUBLICATION_NAME' => 'Gloria Metzger', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-5', 
				'DONOR_PUBLICATION_NAME' => 'City of Seattle ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-6', 
				'DONOR_PUBLICATION_NAME' => 'Pike Place Market PDA ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-7', 
				'DONOR_PUBLICATION_NAME' => 'Friends of the Market ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-8', 
				'DONOR_PUBLICATION_NAME' => 'Friends of the Waterfront ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-9', 
				'DONOR_PUBLICATION_NAME' => 'Russell Investments ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-10', 
				'DONOR_PUBLICATION_NAME' => 'KOMO4 TV ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-11', 
				'DONOR_PUBLICATION_NAME' => 'Slalom Consulting ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-12', 
				'DONOR_PUBLICATION_NAME' => 'Ed & Joan Singler', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-13', 
				'DONOR_PUBLICATION_NAME' => 'Scott Redman', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-14', 
				'DONOR_PUBLICATION_NAME' => 'Janet Hunter', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-15', 
				'DONOR_PUBLICATION_NAME' => 'Rebecca Uusitalo', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-16', 
				'DONOR_PUBLICATION_NAME' => 'Pam and Patrick Smith', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-17', 
				'DONOR_PUBLICATION_NAME' => 'Megan McQueen', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-18', 
				'DONOR_PUBLICATION_NAME' => 'Katheryn Cuyle', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-19', 
				'DONOR_PUBLICATION_NAME' => 'Justine Kim', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-20', 
				'DONOR_PUBLICATION_NAME' => 'Mary and Austin Watson', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-21', 
				'DONOR_PUBLICATION_NAME' => 'Carole Cancler', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-22', 
				'DONOR_PUBLICATION_NAME' => 'Mark Barbieri', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-23', 
				'DONOR_PUBLICATION_NAME' => 'Jeanie and Bruce Nordstrom', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-24', 
				'DONOR_PUBLICATION_NAME' => 'Anita Braker', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-25', 
				'DONOR_PUBLICATION_NAME' => 'Pat and Joe Desimone', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-26', 
				'DONOR_PUBLICATION_NAME' => 'Laura Lippman', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-27', 
				'DONOR_PUBLICATION_NAME' => 'Frankie Loeb', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-28', 
				'DONOR_PUBLICATION_NAME' => 'Mat Witter', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-29', 
				'DONOR_PUBLICATION_NAME' => 'Connie Niva', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-30', 
				'DONOR_PUBLICATION_NAME' => 'Grace Christ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-31', 
				'DONOR_PUBLICATION_NAME' => 'Peter Danelo', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-32', 
				'DONOR_PUBLICATION_NAME' => 'Jean Gardner', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-33', 
				'DONOR_PUBLICATION_NAME' => 'Matt Hanna', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-34', 
				'DONOR_PUBLICATION_NAME' => 'Angela and Ted Leja', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-35', 
				'DONOR_PUBLICATION_NAME' => 'Nancy Usher', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
				$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-36', 
				'DONOR_PUBLICATION_NAME' => 'Tony Otero Marvero', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		//Market Backers
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-37', 
				'DONOR_PUBLICATION_NAME' => 'The Pink Door ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-38', 
				'DONOR_PUBLICATION_NAME' => 'Café Campagne ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-39', 
				'DONOR_PUBLICATION_NAME' => 'Brook Westlund Gallery ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-40', 
				'DONOR_PUBLICATION_NAME' => 'Goldmine Design ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-41', 
				'DONOR_PUBLICATION_NAME' => 'Ghost Alley Espresso ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-42', 
				'DONOR_PUBLICATION_NAME' => 'Indi Chocolates ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-43', 
				'DONOR_PUBLICATION_NAME' => 'Lisa Harris Gallery ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-44', 
				'DONOR_PUBLICATION_NAME' => 'Pike Place Chowder ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-45', 
				'DONOR_PUBLICATION_NAME' => 'Piroshky Piroshky ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-46', 
				'DONOR_PUBLICATION_NAME' => 'Raven’s Nest Treasures ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-47', 
				'DONOR_PUBLICATION_NAME' => 'Reflecting on Seattle ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-48', 
				'DONOR_PUBLICATION_NAME' => 'Sweetie’s Candy ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-49', 
				'DONOR_PUBLICATION_NAME' => 'Uli’s Sausage ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-50', 
				'DONOR_PUBLICATION_NAME' => 'Haley Land ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
		$wpdb->insert( $table_name, array(
				'TRANSACTION_ID' => 'PB-51', 
				'DONOR_PUBLICATION_NAME' => 'Ann Brown ', 
				'TOTAL_HOOFS' => 0, 
				'TOTAL_CHARMS' =>0, 
				'DONATION_AMOUNT' =>0, 
				'CREATED_DATE' => current_time( 'mysql' ),
				'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
				'CREATED_BY' => 'InstallScript'
		));
	}
}