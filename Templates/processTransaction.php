<?php
/*
Template Name: processTransaction
*/
?>
<?php  

insertIntoDBAndReirectPage::addToDB();

class insertIntoDBAndReirectPage{

    public static function addToDB(){

        $transactionId = $_REQUEST['TransactionId'];
        $donorPublicationName = $_REQUEST['PublicationName'];
        $totalHoofs = $_REQUEST['TotalHoofs'];
        $totalCharms = $_REQUEST['TotalCharms'];
        $donationAmount = $_REQUEST['DonationAmount'];

        $transactionId = urldecode($transactionId);
		$donorPublicationName = urldecode($donorPublicationName);
		$totalHoofs = urldecode($totalHoofs);
		$totalCharms = urldecode($totalCharms);
		$donationAmount = urldecode($donationAmount);

		$donorPublicationName = str_replace('\\\'', '\'', $donorPublicationName);

        global $wpdb;

        $table_name = $wpdb->prefix . "pikeup_piggybackers";

        $wpdb->insert( 
                $table_name, 
                array( 
                    'TRANSACTION_ID' => $transactionId, 
                    'DONOR_PUBLICATION_NAME' => $donorPublicationName, 
                    'TOTAL_HOOFS' => $totalHoofs, 
                    'TOTAL_CHARMS' => $totalCharms, 
                    'DONATION_AMOUNT' => $donationAmount, 
                    'CREATED_DATE' => current_time( 'mysql' ),
                    'LAST_MODIFIED_DATE' => current_time( 'mysql' ),
                    'CREATED_BY' => 'pikeup.org'
                )
            );
        $url = './thankyou/';
        header('Location: ' . $url, true, 302);
    }
}

?>


<?php get_header('inner'); ?>
	<div id="content">
		<div class="site-content inner-page-container">
			<div class="main-page">	
		     <?php if(have_posts()):
		          while(have_posts()):the_post(); ?>
				 <div class="inner-page clearfix">		
				 	<h1 class="page-title"><?php  the_title(); ?></h1>		
				 	<?php if ( has_post_thumbnail() ) {
				 		$class="";
						 ?>
					<div class="feature-fig-section">
						<?php $thumbnail_id = get_post_thumbnail_id($post -> ID);
							$thumb = wp_get_attachment_image_src($thumbnail_id, 'full');
							$timthumb = pickup_timthumb_path($thumb[0], 578, 349, '');
		            		 ?> 
							<img src="<?php echo $timthumb; ?>" alt="<?php the_title();?>" title="<?php the_title();?>" />
					</div>
					<?php }
					else{
						$class="full-width";
					}
					 ?>
					<div class="content-section <?php echo $class;?>">
						 <?php the_content(); ?>
					 </div>
				</div> 	
	 			<?php endwhile; ?>
			<?php endif; ?>	
	 		</div>
	   </div>
</div>
<?php get_footer(); ?>