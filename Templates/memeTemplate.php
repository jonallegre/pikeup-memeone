<?php
/**
	Template Name: Meme Frame Theme

*/
?>
<html>
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<style type="text/css">
body  {
    margin: 0; 
    padding: 0;
    text-align: center;
}
.ssba, .memeone_meme, .memeone_image_container{
 display:inline;
}

.memeone_meme{
	max-width: 320px;
	min-width: 320px;
	width: 320px;
}

.memeone_share_container, .memeone_share_text{
 padding:7px;
 font-size: 24px;
}
.bodyclass #memeone_backgrounds { 
    width: 100%;
    margin: 0 auto;
    text-align: center;
}
canvas#memeone_canvas{
	width: 320px !important;
	max-width: 320px !important;
	min-width: 320px !important;	
} 
input#memeone_meme_bottom_text{
	width: 217px;
	height: 30px;
}
.memeone_bottom_input_label{
	font-size: 18px;
}
#memeone_backgrounds_table{
    width: 100%;
	margin: 0 auto;
    text-align: center;
}
#memeone_canvas_placeholder{
    width: 100%;
	margin: 0 auto;
    text-align: center;
}
.memeone_background{
	margin: 0 auto;
    text-align: center;
	display: inline;
	width: 285px;
}
canvas#memeone_canvas
{
    width: 100%;
	margin: 0 auto;
    text-align: center;
}
div#memeone_backgrounds p{
	font-size:24px !important;
}
input[type="button"]{
	background: center #ad172b !important;
    background-color: #ad172b;
    border-radius: 4px;
    color: #fff;
    display: inline-block;
    font-family: "apex_serif_boldregular" !important;;
    font-size: 1.0em !important;
    height: 45px;
    line-height: 45px;
    padding: 0 22px 0 18px !important;
    text-transform: uppercase;
	border:0px;
	margin-top: 1em;
}

input#memeone_submit{
    font-size: 1.8em !important;
    margin-top: 5px;
}

.memeone_bottom_input_label{
width: 100px !important;
}
</style>

<title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
<?php wp_head(); ?>
</head>

<body class="body class">
<img id="MemeFlagOverlayImage" src="../wp-content/uploads/2014/09/FlagMeme.png" style="display:none;"/>
<img id="MemeLinkOverlayImage" src="../wp-content/uploads/2014/09/PikeUpOrgMeme.png" style="display:none;"/>
<?php while (have_posts()) : the_post(); ?>
<div id="container" class="uffes-fullwidth">
			<div id="content" role="main">
					<div id="page-content">
					<?php the_content(); endwhile; ?>
					</div>
			</div>
</div>
</body>
</html>